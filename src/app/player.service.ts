import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from './player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private url: string = "http://localhost:8087/players/all";

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':  'application/json'})
  };

  constructor(private http: HttpClient) { }

  public getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.url);
  }
}
